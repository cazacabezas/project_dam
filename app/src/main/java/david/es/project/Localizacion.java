package david.es.project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Localizacion extends AppCompatActivity implements LocationListener {


    private static final String TAG = Localizacion.class.getSimpleName();

    public static final String INSERT_LOCALIZATION = "http://dabuenano.esy.es/insercion_localization.php";
    public static final String DETALLE_LOCALIZACION = "http://dabuenano.esy.es/detalle_localization.php";


    private TextView messageTextView;
    private TextView messageTextView2;
    private Button but1;
    private Button but2;
    private EditText txtCamion;
    private String truck_number;
    private String msg;
    private JSONObject jobject;
    private HashMap<String, String> map;
    private Gson gson = new Gson();
    private LocationManager locationManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localizacion);


     //   locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);


        messageTextView = (TextView) findViewById(R.id.txtview);
        messageTextView2 = (TextView) findViewById(R.id.messageTextView2);
        txtCamion = (EditText) findViewById(R.id.etxCamion);
        but1 = (Button) findViewById(R.id.but1);
        //but2 = (Button) findViewById(R.id.butMaps);

       // try {
       //     locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
       //             2000,
       //             10, this);
       // }
       // catch(SecurityException e){
        //     e.printStackTrace();
       // }
    }

    public void aceptar (View v){

        try {

            double longitud = Double.parseDouble((String) messageTextView.getText());
            double latitud = Double.parseDouble((String) messageTextView2.getText());


            locationManager = null;

            Intent i = new Intent(this, MapsActivity.class);
            i.putExtra("longitud", longitud);
            i.putExtra("latitud", latitud);
            startActivity(i);

        }
        catch(Exception e){
            e.printStackTrace();
        }
        //this.onStop();



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onLocationChanged(Location location) {

       // msg = "" + location.getLatitude()
         //       + "/" + location.getLongitude();

        String msg2 = "" + location.getLongitude();
        String msg3 = "" + location.getLatitude();

        messageTextView.setText(msg2);
        messageTextView2.setText(msg3);






        //Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onProviderDisabled(String provider) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getBaseContext(), "Gps is turned off!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

        Toast.makeText(getBaseContext(), "Gps is turned on!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    public void buscarIdCamion(View v){

        //map = new HashMap<>();// Mapeo previo

        //map.put("username", username);

        // Crear nuevo objeto Json basado en el mapa
        //jobject = new JSONObject(map);
        String numero = txtCamion.getText().toString();

        String newURL = DETALLE_LOCALIZACION + "?numero=" + numero;

        // Depurando objeto Json...
        //Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.GET,
                        newURL,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                // Procesar la respuesta del servidor
                                procesarRespuesta(response);


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );




    }

    private void procesarRespuesta(JSONObject response) {

        try {
            // Obtener atributo "mensaje"
            String mensaje = response.getString("estado");

            switch (mensaje) {
                case "1":
                    // Obtener objeto "meta"
                    JSONObject object = response.getJSONObject("truck");

                    //Parsear objeto
                    Localization localization = gson.fromJson(object.toString(), Localization.class);

                    truck_number = localization.getId();
                    Intent intent = new Intent(this, EnviarLocalizacion.class);
                    intent.putExtra("truck_id", truck_number);
                    intent.putExtra("coordinate", messageTextView.getText().toString() + messageTextView2.getText().toString());
                    startActivity(intent);
                    //work_place_id = ids.getWork_place_id();



                    break;

                case "2":
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;

                case "3":
                    String mensaje3 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje3,
                            Toast.LENGTH_LONG).show();
                    break;
            }

            //return ids2;


        } catch (JSONException e) {

            e.printStackTrace();
        }
        //return ids;
    }


    }
