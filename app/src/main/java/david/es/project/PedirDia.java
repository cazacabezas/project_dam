package david.es.project;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PedirDia extends AppCompatActivity {


    private static final String TAG = PedirDia.class.getSimpleName();
    public static final String UPDATE_PROVIDE_DAY = "http://dabuenano.esy.es/actualizacion_provide_day.php";

    private HashMap<String, String> map;
    private JSONObject jobject;
    private Button but1;
    private String id;
    private String provide_worker_id;
    private String provide_worker_work_place_id;
    private String username;
    private String search_worker_id;
    private String search_worker_work_place_id;
    private String day;

    private Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedir_dia);


        but1 = (Button) findViewById(R.id.but1);

        //buscarIdUser(username);
        //recojo los parametros recogidos por la activity
        Bundle bundle = getIntent().getExtras();
        id=bundle.getString("id");
        provide_worker_id = bundle.getString("provide_worker_id");
        provide_worker_work_place_id = bundle.getString("provide_worker_work_place_id");
        day = bundle.getString("day");

        //los dos siguientes son el id y el worker_palce_id del trabajador que esta usando la app
        search_worker_id = bundle.getString("search_worker_id");
        search_worker_work_place_id = bundle.getString("search_worker_work_place_id");
    }

    public void aceptar(View v) {



        //date = date_String.getText().toString().trim();

        map = new HashMap<>();// Mapeo previo

        map.put("id", id);
        map.put("day", day);
        map.put("provide_worker_id", provide_worker_id);
        map.put("provide_worker_work_place_id", provide_worker_work_place_id);
        map.put("search_worker_id", search_worker_id);
        map.put("search_worker_work_place_id", search_worker_work_place_id);

        // Crear nuevo objeto Json basado en el mapa
        jobject = new JSONObject(map);

        // Depurando objeto Json...
        Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        UPDATE_PROVIDE_DAY,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Procesar la respuesta del servidor
                                procesarRespuesta2(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );}

    private void procesarRespuesta2(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    //openProfile();
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de éxito
                    //this.setResult(Activity.RESULT_OK);
                    // Terminar actividad
                    //this.finish();
                    //openProfile();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    this.setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    this.finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }




}
