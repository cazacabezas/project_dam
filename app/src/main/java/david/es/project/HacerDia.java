package david.es.project;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class HacerDia extends AppCompatActivity {

    private static final String TAG = HacerDia.class.getSimpleName();
    public static final String UPDATE_SEARCH_DAY = "http://dabuenano.esy.es/actualizacion_search_day.php";
    public static final String WORKER_IDS_URL = "http://dabuenano.esy.es/detalle_worker_username.php";

    public static final String PREFS_NAME = "AOP_PREFS";
    public static final String PREFS_KEY = "usuario";

    private TextView txtPrueba;

    private HashMap<String, String> map;
    private JSONObject jobject;
    private Button but1;
    private String id;
    private String search_worker_id;
    private String search_worker_work_place_id;
    private String username;
    private String offer_worker_id1;
    private String offer_worker_work_place_id1;
    private String date;

    private Gson gson = new Gson();
    private Ids ids = new Ids();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hacer_dia);

        SharedPreferences prefe=getSharedPreferences("datos",Context.MODE_PRIVATE);

        username = prefe.getString("username", "");


        but1 = (Button) findViewById(R.id.but1);

        //buscarIdUser(username);
        //recojo los parametros recogidos por la activity
        Bundle bundle = getIntent().getExtras();
        id=bundle.getString("id");
        search_worker_id = bundle.getString("search_worker_id");
        search_worker_work_place_id = bundle.getString("search_worker_work_place_id");
        date = bundle.getString("day");
        offer_worker_id1 = bundle.getString("offer_worker_id1");
        offer_worker_work_place_id1 = bundle.getString("offer_worker_work_place_id1");

        //txtPrueba = (TextView) findViewById(R.id.txtPrueba);



        //mote = getValue(this);


       // txtPrueba.setText(prefe.getString("username",""));
        //txtPrueba.setText(mote);






    }

  /*  public void buscarIdUser(String username){

        //map = new HashMap<>();// Mapeo previo

        //map.put("username", username);

        // Crear nuevo objeto Json basado en el mapa
        //jobject = new JSONObject(map);

        String newURL = WORKER_IDS_URL + "?username=" + username;

        // Depurando objeto Json...
        //Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.GET,
                        newURL,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                // Procesar la respuesta del servidor
                                procesarRespuesta(response);


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );




    }*/

  /*  private void procesarRespuesta(JSONObject response) {

        try {
            // Obtener atributo "mensaje"
            String mensaje = response.getString("estado");

            switch (mensaje) {
                case "1":
                    // Obtener objeto "meta"
                    JSONObject object = response.getJSONObject("ids");

                    //Parsear objeto
                    ids = gson.fromJson(object.toString(), Ids.class);

                    offer_worker_id1 = ids.getId();
                    offer_worker_work_place_id1 = ids.getWork_place_id();

                    /*Toast toast = Toast.makeText(this, id, Toast.LENGTH_SHORT);
                    toast.show();
                    Toast toasta = Toast.makeText(this, work_place_id, Toast.LENGTH_SHORT);
                    toasta.show();*/

                    // String id = ids.getId();
                    // String work_place_id = ids.getWork_place_id();

                    // ids2 = new Ids(id,work_place_id);
                    // return ids2;

                    // Asignar color del fondo
                   /* switch (ids.getId()) {
                        case "Salud":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.saludColor));
                            break;
                        case "Finanzas":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.finanzasColor));
                            break;
                        case "Espiritual":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.espiritualColor));
                            break;
                        case "Profesional":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.profesionalColor));
                            break;
                        case "Material":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.materialColor));
                            break;
                    }

                    // Seteando valores en los views
                    titulo.setText(meta.getTitulo());
                    descripcion.setText(meta.getDescripcion());
                    prioridad.setText(meta.getPrioridad());
                    fechaLim.setText(meta.getFechaLim());
                    categoria.setText(meta.getCategoria());*/

                   /* break;

                case "2":
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;

                case "3":
                    String mensaje3 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje3,
                            Toast.LENGTH_LONG).show();
                    break;
            }

            //return ids2;


        } catch (JSONException e) {

            e.printStackTrace();
        }
        //return ids;
    }*/

    public void aceptar(View v) {



        //date = date_String.getText().toString().trim();

        map = new HashMap<>();// Mapeo previo

        map.put("id", id);
        map.put("day", date);
        map.put("search_worker_id", search_worker_id);
        map.put("search_worker_work_place_id", search_worker_work_place_id);
        map.put("offer_worker_id1", offer_worker_id1);
        map.put("offer_worker_work_place_id1", offer_worker_work_place_id1);

        // Crear nuevo objeto Json basado en el mapa
        jobject = new JSONObject(map);

        // Depurando objeto Json...
        Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        UPDATE_SEARCH_DAY,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Procesar la respuesta del servidor
                                procesarRespuesta2(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );




        /*

        username = editTextUsername.getText().toString().trim();
        password = editTextPassword.getText().toString().trim();

        HashMap<String, String> map = new HashMap<>();// Mapeo previo

        map.put("username", username);
        map.put("password", password);

        // Crear nuevo objeto Json basado en el mapa
        JSONObject jobject = new JSONObject(map);

        // Depurando objeto Json...
        Log.d(TAG, jobject.toString());


        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        LOGIN_URL,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Procesar la respuesta del servidor
                                procesarRespuesta(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );*/

    }

    private void procesarRespuesta2(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    //openProfile();
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de éxito
                    //this.setResult(Activity.RESULT_OK);
                    // Terminar actividad
                    //this.finish();
                    //openProfile();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    this.setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    this.finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }




    public String getValue(Context context) {
        SharedPreferences settings;
        String text;
        settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        text = settings.getString(PREFS_KEY, null); //2
        return text;
    }
}
