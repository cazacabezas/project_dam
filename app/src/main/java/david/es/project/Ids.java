package david.es.project;

/**
 * Created by David on 25/5/16.
 */
public class Ids {

    private String id;
    private String work_place_id;

    public Ids() {
    }

    public Ids(String id, String work_place_id) {
        this.id = id;
        this.work_place_id = work_place_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWork_place_id() {
        return work_place_id;
    }

    public void setWork_place_id(String work_place_id) {
        this.work_place_id = work_place_id;
    }
}
