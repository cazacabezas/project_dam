package david.es.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Pantalla_inicio extends AppCompatActivity {

    private TextView tview;
    private String username;

    private Button but1;
    private Button but2;
    private Button but3;
    private Button but4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_inicio);

        //recojo los parametros recogidos por la activity
        Bundle bundle = getIntent().getExtras();
        username=bundle.getString("username");

        but1 = (Button) findViewById(R.id.but1);
        but2 = (Button) findViewById(R.id.but2);
        but3 = (Button) findViewById(R.id.but3);
        but4 = (Button) findViewById(R.id.but4);

        tview = (TextView) findViewById(R.id.textView9);


        //tview.setText(username);

       // webView1.loadUrl("http://" + dato);
    }

    public void insertDate(View v){

        Intent intent = new Intent(this, InsertDate.class);
        intent.putExtra("username", username);
        startActivity(intent);

    }

    public void insertProvideDay(View v){
        Intent intent = new Intent(this, InsertProvideDate.class);
        intent.putExtra("username", username);
        startActivity(intent);
    }

    public void detailtruck(View v){
        Intent intent = new Intent(this, Estado_Camion.class);
        intent.putExtra("username", username);
        startActivity(intent);

    }

    public void localizacion(View v){
        Intent intent = new Intent(this, Localizacion.class);
        intent.putExtra("username", username);
        startActivity(intent);
    }

   /* public void listaDiasOfertados(View v){
        Intent intent = new Intent(this, ListaDiasOfertados.class);
        intent.putExtra("username", username);
        startActivity(intent);
    }*/
}
