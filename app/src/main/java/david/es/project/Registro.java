package david.es.project;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Registro extends AppCompatActivity {

    private static final String TAG = Registro.class.getSimpleName();
    public static final String LOGIN_URL = "http://dabuenano.esy.es/insercion_worker.php";

    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextName;
    private EditText editTextLast_name;
    private EditText editTextPhone;
    private EditText editTextCategory;
    private Spinner spinner;
    private Button but1;

    private String username;
    private String password;
    private String first_name;
    private String last_name;
    private String phone;
    private String categoria;
    private String selec;
    private String work_place_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        editTextUsername = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextLast_name = (EditText) findViewById(R.id.editTextLast_Name);
        editTextCategory =(EditText) findViewById(R.id.editTextCategory);
        editTextPhone = (EditText) findViewById((R.id.editTextPhone));
        spinner = (Spinner) findViewById(R.id.spinner);
        String []opciones={"Parque de la Resina","Parque de Vallecas","Parque de Manoteras"};
        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_spinner_item, opciones);
        spinner.setAdapter(adapter);

        but1 = (Button) findViewById(R.id.but1);


    }

    public void Login(View v){

        username = editTextUsername.getText().toString().trim();
        password = editTextPassword.getText().toString().trim();
        first_name = editTextName.getText().toString().trim();
        last_name = editTextLast_name.getText().toString().trim();
        phone = editTextPhone.getText().toString().trim();
        categoria = editTextCategory.getText().toString().trim();
        selec = spinner.getSelectedItem().toString();

        if(selec.equals("Parque de Vallecas")){

            work_place_id = "1";
        }
        if(selec.equals("Parque de La Resina")){

            work_place_id = "2";
        }

        if(selec.equals("Parque de Manoteras")) {

            work_place_id ="3";

        }

        HashMap<String, String> map = new HashMap<>();// Mapeo previo

        map.put("username", username);
        map.put("password", password);
        map.put("first_name", first_name);
        map.put("last_name", last_name);
        map.put("phone", phone);
        map.put("categoria", categoria);
        map.put("work_place_id", work_place_id);


        // Crear nuevo objeto Json basado en el mapa
        JSONObject jobject = new JSONObject(map);

        // Depurando objeto Json...
        Log.d(TAG, jobject.toString());


        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        LOGIN_URL,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Procesar la respuesta del servidor
                                procesarRespuesta(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );

    }

    private void procesarRespuesta(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    //openProfile();
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de éxito
                    //this.setResult(Activity.RESULT_OK);
                    // Terminar actividad
                    //this.finish();
                    openProfile();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    this.setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    this.finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void openProfile(){
        Intent intent = new Intent(this, Pantalla_inicio.class);
        //intent.putExtra(KEY_USERNAME, username);
        startActivity(intent);
    }


}
