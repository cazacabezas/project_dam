package david.es.project;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class ListaDiasOfertados extends AppCompatActivity {

    ListView listview2;
    ProvideDayAdapter arrayAdapterprovide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_dias_ofertados);

        // Obtener instancia de la lista
        listview2= (ListView) findViewById(R.id.listView2);

        // Crear adaptador y setear
        arrayAdapterprovide = new ProvideDayAdapter(this);
        listview2.setAdapter(arrayAdapterprovide);
    }
}
