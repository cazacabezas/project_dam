package david.es.project;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 25/5/16.
 */
public class ProvideDayAdapter extends ArrayAdapter {

    // Atributos
    private RequestQueue requestQueue;
    JsonObjectRequest jsArrayRequest;
    private static final String URL_BASE = "http://dabuenano.esy.es";
    private static final String URL_JSON = "/obtener_provide_day.php";
    private static final String TAG = "ProvideDayAdapter";
    List<ProvideDay> items;

    public ProvideDayAdapter(Context context) {
        super(context, 0);

        // Crear nueva cola de peticiones
        requestQueue= Volley.newRequestQueue(context);

        // Nueva petición JSONObject
        jsArrayRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL_BASE + URL_JSON,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        items = parseJson(response);
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Respuesta en JSON: " + error.getMessage());

                    }
                }
        );

        // Añadir petición a la cola
        requestQueue.add(jsArrayRequest);
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        // Referencia del view procesado
        View listItemView;

        //Comprobando si el View no existe
        listItemView = null == convertView ? layoutInflater.inflate(
                R.layout.provideday,
                parent,
                false) : convertView;


        // Obtener el item actual
        ProvideDay item = items.get(position);

        // Obtener Views
        TextView textoDate = (TextView) listItemView.
                findViewById(R.id.textView);
        TextView textoWorkerId = (TextView) listItemView.
                findViewById(R.id.textView2);
        //final ImageView imagenPost = (ImageView) listItemView.
        //        findViewById(R.id.imagenPost);

        // Actualizar los Views
        textoDate.setText(item.getDay());
        textoWorkerId.setText(item.getProvide_worker_id());

        // Petición para obtener la imagen
        /*ImageRequest request = new ImageRequest(
                URL_BASE + item.getImagen(),
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imagenPost.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imagenPost.setImageResource(R.drawable.error);
                        Log.d(TAG, "Error en respuesta Bitmap: "+ error.getMessage());
                    }
                }); */

        // Añadir petición a la cola
        //requestQueue.add(request);


        return listItemView;
    }

    public List<ProvideDay> parseJson(JSONObject jsonObject){
        // Variables locales
        List<ProvideDay> providedays = new ArrayList<>();
        JSONArray jsonArray= null;

        try {
            // Obtener el array del objeto
            jsonArray = jsonObject.getJSONArray("provide_days");

            for(int i=0; i<jsonArray.length(); i++){

                try {
                    JSONObject objeto= jsonArray.getJSONObject(i);

                    ProvideDay provideday= new ProvideDay(
                            objeto.getString("id"),
                            objeto.getString("day"),
                            objeto.getString("provide_worker_id"),
                            objeto.getString("provide_worker_work_place_id"),
                            objeto.getString("search_worker_id"),
                            objeto.getString("offer_worker_work_place_id"));


                    providedays.add(provideday);

                } catch (JSONException e) {
                    Log.e(TAG, "Error de parsing: "+ e.getMessage());
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return providedays;
    }




}
