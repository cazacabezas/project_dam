package david.es.project;

/**
 * Created by David on 24/5/16.
 */
public class Truck {

    String id;
    String number;
    String state;
    String work_place_id;

    public Truck() {
    }

    public Truck(String id, String number, String state, String work_place_id) {
        this.id = id;
        this.number = number;
        this.state = state;
        this.work_place_id = work_place_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWork_place_id() {
        return work_place_id;
    }

    public void setWork_place_id(String work_place_id) {
        this.work_place_id = work_place_id;
    }
}
