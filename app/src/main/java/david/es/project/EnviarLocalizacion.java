package david.es.project;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EnviarLocalizacion extends AppCompatActivity {


    private static final String TAG = EnviarLocalizacion.class.getSimpleName();

    public static final String INSERT_LOCALIZATION = "http://dabuenano.esy.es/insercion_localization.php";


    private String truck_id;
    private String cordinate;
    private Button but1;

    private JSONObject jobject;
    private HashMap<String, String> map;
    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enviar_localizacion);

        but1 = (Button) findViewById(R.id.but1);
        Bundle bundle = getIntent().getExtras();
        truck_id=bundle.getString("truck_id");
        cordinate = bundle.getString("coordinate");
    }

    public void insertar(View v) {


        // buscarIdCamion(txtCamion.getText().toString());
        // date = date_String.getText().toString().trim();

        //String coordinate = messageTextView.getText().toString() +"/" + messageTextView2.getText().toString();
        map = new HashMap<>();// Mapeo previo

        map.put("truck_id", truck_id);
        map.put("coordinate", cordinate);

        //map.put("search_", work_place_id);
        //map2.put("offer_worker_id1", "");
        //map2.put("offer_worker_work_place_id1", "");

        // Crear nuevo objeto Json basado en el mapa
        jobject = new JSONObject(map);

        // Depurando objeto Json...
        Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        INSERT_LOCALIZATION,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Procesar la respuesta del servidor
                                procesarRespuesta2(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );


    }

    private void procesarRespuesta2(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    //openProfile();
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de éxito
                    //this.setResult(Activity.RESULT_OK);
                    // Terminar actividad
                    //this.finish();
                    //openProfile();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    this.setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    this.finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
