package david.es.project;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 24/5/16.
 */
public class WorkerAdapter extends ArrayAdapter {


    // Atributos
    private RequestQueue requestQueue;
    JsonObjectRequest jsArrayRequest;
    private static final String URL_BASE = "http://dabuenano.esy.es";
    private static final String URL_JSON = "/obtener_workers.php";
    private static final String TAG = "WorkerAdapter";
    List<Worker> items;

    public WorkerAdapter(Context context) {
        super(context, 0);

        // Crear nueva cola de peticiones
        requestQueue= Volley.newRequestQueue(context);

        // Nueva petición JSONObject
        jsArrayRequest = new JsonObjectRequest(
                Request.Method.GET,
                URL_BASE + URL_JSON,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        items = parseJson(response);
                        notifyDataSetChanged();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "Error Respuesta en JSON: " + error.getMessage());

                    }
                }
        );

        // Añadir petición a la cola
        requestQueue.add(jsArrayRequest);
    }

    @Override
    public int getCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        // Referencia del view procesado
        View listItemView;

        //Comprobando si el View no existe
        listItemView = null == convertView ? layoutInflater.inflate(
                R.layout.worker,
                parent,
                false) : convertView;


        // Obtener el item actual
        Worker item = items.get(position);

        // Obtener Views
        TextView textoNombre = (TextView) listItemView.
                findViewById(R.id.textView);
        TextView textoUsername = (TextView) listItemView.
                findViewById(R.id.textView2);
        //final ImageView imagenPost = (ImageView) listItemView.
        //        findViewById(R.id.imagenPost);

        // Actualizar los Views
        textoNombre.setText(item.getFirst_name());
        textoUsername.setText(item.getUsername());

        // Petición para obtener la imagen
        /*ImageRequest request = new ImageRequest(
                URL_BASE + item.getImagen(),
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        imagenPost.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        imagenPost.setImageResource(R.drawable.error);
                        Log.d(TAG, "Error en respuesta Bitmap: "+ error.getMessage());
                    }
                }); */

        // Añadir petición a la cola
        //requestQueue.add(request);


        return listItemView;
    }

    public List<Worker> parseJson(JSONObject jsonObject){
        // Variables locales
        List<Worker> workers = new ArrayList<>();
        JSONArray jsonArray= null;

        try {
            // Obtener el array del objeto
            jsonArray = jsonObject.getJSONArray("workers");

            for(int i=0; i<jsonArray.length(); i++){

                try {
                    JSONObject objeto= jsonArray.getJSONObject(i);

                    Worker worker= new Worker(
                            objeto.getString("id"),
                            objeto.getString("first_name"),
                            objeto.getString("last_name"),
                            objeto.getString("password"),
                            objeto.getString("categoria"),
                            objeto.getString("phone"),
                            objeto.getString("work_place_id"),
                            objeto.getString("username"));


                    workers.add(worker);

                } catch (JSONException e) {
                    Log.e(TAG, "Error de parsing: "+ e.getMessage());
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return workers;
    }


}
