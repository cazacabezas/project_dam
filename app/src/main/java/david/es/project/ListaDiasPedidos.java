package david.es.project;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListaDiasPedidos extends AppCompatActivity {

    ViewHolder holder;
    ListView listview;
    ArrayAdapter arrayAdapter;
    private String username;
   // private TextView txtView1;
   // private TextView textView2;
    private String value;
    //private SharedPreferences preferenceSettings;
    //private SharedPreferences.Editor preferenceEditor;
    public static final String PREFS_NAME = "AOP_PREFS";
    public static final String PREFS_KEY = "username";


    // Atributos
    private RequestQueue requestQueue;
    JsonObjectRequest jsArrayRequest;
    private static final String URL_BASE = "http://dabuenano.esy.es";
    private static final String URL_JSON = "/obtener_search_day.php";
    private static final String TAG = "SearchDayAdapter";
    private String search_worker_work_place_id;
    private String id;
    List<SearchDay> items;
    TextView textoDate;
    TextView textoWorkerId;


    private static final int PREFERENCE_MODE_PRIVATE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_dias_pedidos);


        //recojo los parametros recogidos por la activity
        Bundle bundle = getIntent().getExtras();
        username=bundle.getString("username");

       // SharedPreferences prefe=getSharedPreferences("datos",Context.MODE_PRIVATE);
       // et1.setText(prefe.getString("mail",""));

        SharedPreferences preferencias=getSharedPreferences("datos",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString("username", username.toString());
        editor.commit();
        //finish();
        //save(this, username);
        //preferenceEditor.putString("username", username);
        //preferenceEditor.commit();
        //Obtener instancia de la lista
        listview= (ListView) findViewById(R.id.listView);
        //txtView1 = (TextView) findViewById(R.id.textView2);

        // Crear adaptador y setear
        arrayAdapter = new SearchDayAdapter(this);
        listview.setAdapter(arrayAdapter);

//        Toast.makeText(getApplicationContext(),arrayAdapter.getCount(),Toast.LENGTH_SHORT).show();


       /* listview.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Object obj = (Object)arrayAdapter.getItem(position);

                        String fecha = textoDate.getText().toString();
                        //String msg = "Elegiste la tarea:n"+tareaActual.getDay()+"-"+tareaActual.getOffer_worker_id1();
                        Toast.makeText(getApplicationContext(),fecha,Toast.LENGTH_SHORT).show();
                    }


                });*/
        /*listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Object o = arg0.getItemAtPosition(position);
                String str = (String) o;//As you are using Default String Adapter
                Toast.makeText(getApplicationContext(), str, Toast.LENGTH_SHORT).show();
            }
        });*/



       // Toast notificacion=Toast.makeText(this,text, Toast.LENGTH_LONG);
       // notificacion.show();

    }

    private class SearchDayAdapter extends ArrayAdapter {





        public SearchDayAdapter(Context context) {
            super(context, 0);

            // Crear nueva cola de peticiones
            requestQueue= Volley.newRequestQueue(context);

            // Nueva petición JSONObject
            jsArrayRequest = new JsonObjectRequest(
                    Request.Method.GET,
                    URL_BASE + URL_JSON,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            items = parseJson(response);
                            notifyDataSetChanged();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d(TAG, "Error Respuesta en JSON: " + error.getMessage());

                        }
                    }
            );

            // Añadir petición a la cola
            requestQueue.add(jsArrayRequest);
        }

        @Override
        public int getCount() {
            return items != null ? items.size() : 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

            // Referencia del view procesado
             View listItemView;
          //  holder = new ViewHolder();

            //Comprobando si el View no existe
            listItemView = null == convertView ? layoutInflater.inflate(
                    R.layout.searchday,
                    parent,
                    false) : convertView;


            // Obtener el item actual
            SearchDay item = items.get(position);

            // Obtener Views
            textoDate = (TextView) listItemView.
                    findViewById(R.id.textView);
           // textoDate.setTag("fecha");
            textoWorkerId = (TextView) listItemView.
                    findViewById(R.id.textView2);
            //final ImageView imagenPost = (ImageView) listItemView.
            //        findViewById(R.id.imagenPost);

            // Actualizar los Views
            textoDate.setText(item.getDay());
            textoWorkerId.setText(item.getSearch_worker_id());
           // search_worker_work_place_id = item.getSearch_worker_work_place_id();
           // id = item.getId();



            return listItemView;
        }

      /*  private View.OnClickListener mOnTitleClickListener = new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int name=(int)v.getTag();
                Log.v("TAG","Name :"+name);
            }
        };*/


        public List<SearchDay> parseJson(JSONObject jsonObject){
            // Variables locales
            List<SearchDay> searchdays = new ArrayList<>();
            JSONArray jsonArray= null;

            try {
                // Obtener el array del objeto
                jsonArray = jsonObject.getJSONArray("search_days");

                for(int i=0; i<jsonArray.length(); i++){

                    try {
                        JSONObject objeto= jsonArray.getJSONObject(i);

                        SearchDay searchday= new SearchDay(
                                objeto.getString("id"),
                                objeto.getString("day"),
                                objeto.getString("search_worker_id"),
                                objeto.getString("search_worker_work_place_id"),
                                objeto.getString("offer_worker_id1"),
                                objeto.getString("offer_worker_work_place_id1"));


                        searchdays.add(searchday);

                    } catch (JSONException e) {
                        Log.e(TAG, "Error de parsing: "+ e.getMessage());
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return searchdays;
        }



    }


}


