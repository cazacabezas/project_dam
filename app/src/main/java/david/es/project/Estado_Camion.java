package david.es.project;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Estado_Camion extends AppCompatActivity {

    private Button but1;
    private EditText editTextCamion;
    private TextView txtEstadoCamion;
    private Gson gson = new Gson();

    private String numero;
    private String work_place_id;
    private String state;
    private String name;


    private static final String TAG = Estado_Camion.class.getSimpleName();
    public static final String TRUCK_DETAIL = "http://dabuenano.esy.es/detalle_truck.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estado__camion);

        but1 = (Button) findViewById(R.id.but1);
        editTextCamion = (EditText) findViewById(R.id.editTextCamion);
        txtEstadoCamion = (TextView) findViewById(R.id.txtEstadoCamion);

//        String camion = editTextCamion.getText().toString();


    }

    public void buscarCamion(View v){

        //map = new HashMap<>();// Mapeo previo

        //map.put("username", username);

        // Crear nuevo objeto Json basado en el mapa
        //jobject = new JSONObject(map);


        String camion = editTextCamion.getText().toString();
        String newURL = TRUCK_DETAIL + "?numero=" + camion;

        // Depurando objeto Json...
        //Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.GET,
                        newURL,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                // Procesar la respuesta del servidor
                                procesarRespuesta3(response);


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );




    }

    private void procesarRespuesta3(JSONObject response) {

        try {
            // Obtener atributo "mensaje"
            String mensaje = response.getString("estado");

            switch (mensaje) {
                case "1":
                    // Obtener objeto "meta"
                    JSONObject object = response.getJSONObject("truck");

                    //Parsear objeto
                    Truck truck = gson.fromJson(object.toString(), Truck.class);

                   // id = ids.getId();
                   // work_place_id = ids.getWork_place_id();

                    numero = truck.getNumber();
                    state = truck.getState();

                    txtEstadoCamion.setText(state);

                    /*Toast toast = Toast.makeText(this, id, Toast.LENGTH_SHORT);
                    toast.show();
                    Toast toasta = Toast.makeText(this, work_place_id, Toast.LENGTH_SHORT);
                    toasta.show();*/

                    // String id = ids.getId();
                    // String work_place_id = ids.getWork_place_id();

                    // ids2 = new Ids(id,work_place_id);
                    // return ids2;

                    // Asignar color del fondo
                   /* switch (ids.getId()) {
                        case "Salud":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.saludColor));
                            break;
                        case "Finanzas":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.finanzasColor));
                            break;
                        case "Espiritual":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.espiritualColor));
                            break;
                        case "Profesional":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.profesionalColor));
                            break;
                        case "Material":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.materialColor));
                            break;
                    }

                    // Seteando valores en los views
                    titulo.setText(meta.getTitulo());
                    descripcion.setText(meta.getDescripcion());
                    prioridad.setText(meta.getPrioridad());
                    fechaLim.setText(meta.getFechaLim());
                    categoria.setText(meta.getCategoria());*/

                    break;

                case "2":
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;

                case "3":
                    String mensaje3 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje3,
                            Toast.LENGTH_LONG).show();
                    break;
            }

            //return ids2;


        } catch (JSONException e) {

            e.printStackTrace();
        }
        //return ids;
    }

}
