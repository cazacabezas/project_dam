package david.es.project;

/**
 * Created by David on 24/5/16.
 */
public class Worker {

    private String id;
    private String first_name;
    private String last_name;
    private String password;
    private String categoria;
    private String phone;
    private String Work_place_id;
    private String username;

    public Worker() {
    }

    public Worker(String id, String first_name, String last_name, String password, String categoria, String phone, String work_place_id, String username) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.password = password;
        this.categoria = categoria;
        this.phone = phone;
        this.Work_place_id = work_place_id;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWork_place_id() {
        return Work_place_id;
    }

    public void setWork_place_id(String work_place_id) {
        Work_place_id = work_place_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
