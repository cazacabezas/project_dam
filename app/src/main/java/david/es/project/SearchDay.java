package david.es.project;

/**
 * Created by David on 24/5/16.
 */
public class SearchDay {

    String id;
    String day;
    String search_worker_id;
    String search_worker_work_place_id;
    String offer_worker_id1;
    String offer_worker_work_place_id1;

    public SearchDay() {
    }

    public SearchDay(String id, String day, String search_worker_id, String search_worker_work_place_id, String offer_worker_id1, String offer_worker_work_place_id1) {
        this.id = id;
        this.day = day;
        this.search_worker_id = search_worker_id;
        this.search_worker_work_place_id = search_worker_work_place_id;
        this.offer_worker_id1 = offer_worker_id1;
        this.offer_worker_work_place_id1 = offer_worker_work_place_id1;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getSearch_worker_id() {
        return search_worker_id;
    }

    public void setSearch_worker_id(String search_worker_id) {
        this.search_worker_id = search_worker_id;
    }

    public String getSearch_worker_work_place_id() {
        return search_worker_work_place_id;
    }

    public void setSearch_worker_work_place_id(String search_worker_work_place_id) {
        this.search_worker_work_place_id = search_worker_work_place_id;
    }

    public String getOffer_worker_id1() {
        return offer_worker_id1;
    }

    public void setOffer_worker_id1(String offer_worker_id1) {
        this.offer_worker_id1 = offer_worker_id1;
    }

    public String getOffer_worker_work_place_id1() {
        return offer_worker_work_place_id1;
    }

    public void setOffer_worker_work_place_id1(String offer_worker_work_place_id1) {
        this.offer_worker_work_place_id1 = offer_worker_work_place_id1;
    }
}
