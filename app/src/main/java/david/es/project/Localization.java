package david.es.project;

/**
 * Created by David on 24/5/16.
 */
public class Localization {

    String id;
    String coordinate;
    String truck_id;
    String truck_work_place_id;
    String numero;

    public Localization() {
    }

    public Localization(String id, String coordinate, String truck_id, String truck_work_place_id, String numero) {
        this.id = id;
        this.coordinate = coordinate;
        this.truck_id = truck_id;
        this.truck_work_place_id = truck_work_place_id;
        this.numero = numero;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(String coordinate) {
        this.coordinate = coordinate;
    }

    public String getTruck_id() {
        return truck_id;
    }

    public void setTruck_id(String truck_id) {
        this.truck_id = truck_id;
    }

    public String getTruck_work_place_id() {
        return truck_work_place_id;
    }

    public void setTruck_work_place_id(String truck_work_place_id) {
        this.truck_work_place_id = truck_work_place_id;
    }
}
