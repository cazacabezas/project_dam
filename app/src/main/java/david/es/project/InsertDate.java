package david.es.project;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class InsertDate extends AppCompatActivity {

    private static final String TAG = InsertDate.class.getSimpleName();
    public static final String WORKER_IDS_URL = "http://dabuenano.esy.es/detalle_worker_username.php";

    public static final String INSERT_DATE = "http://dabuenano.esy.es/insercion_search_day.php";

    public static final String SEARCH_DATE = "http://dabuenano.esy.es/dia_search_day.php";

    private String username;
    private CalendarView calV;
    private Button but1;
    private Button but2;
    private Button but3;
    private String date;
    private TextView date_String;
    private Gson gson = new Gson();
    private Ids ids = new Ids();
    private Ids ids3 = new Ids();
    private SearchDay sday = new SearchDay();
    private HashMap<String, String> map;
    private HashMap<String, String> map2;
    private JSONObject jobject;
    private JSONObject jobject2;


    private String id;
    private String work_place_id;

    private String id_searchday;
    private String day;
    private String search_worker_id;
    private String search_worker_work_place_id;
    private String offer_worker_id1;
    private String offer_worker_work_place_id1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert_date);

        Bundle bundle = getIntent().getExtras();
        username=bundle.getString("username");

        buscarIdUser(username);

        calV = (CalendarView) findViewById(R.id.calView);
        but1 = (Button) findViewById(R.id.but1);
        but2 = (Button) findViewById(R.id.but2);
        but3 = (Button) findViewById(R.id.but3);
        date_String = (TextView) findViewById(R.id.txtDate);

        calV.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

                    //show the selected date as a toast

            @Override

            public void onSelectedDayChange(CalendarView view, int year, int month, int day) {

                //Toast.makeText(getApplicationContext(), day + "/" + month + "/" + year, Toast.LENGTH_LONG).show();
                date = year + "-" + (month +1) + "-" + day;
                date_String.setText(date);
                //return date;

            }

        });



    }

    public void buscarIdUser(String username){

        //map = new HashMap<>();// Mapeo previo

        //map.put("username", username);

        // Crear nuevo objeto Json basado en el mapa
        //jobject = new JSONObject(map);

        String newURL = WORKER_IDS_URL + "?username=" + username;

        // Depurando objeto Json...
        //Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.GET,
                        newURL,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                // Procesar la respuesta del servidor
                                procesarRespuesta(response);


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );




    }


    public void buscarDiaaHaceraCompi(View v){

        //map = new HashMap<>();// Mapeo previo

        //map.put("username", username);

        // Crear nuevo objeto Json basado en el mapa
        //jobject = new JSONObject(map);

        String newURL = SEARCH_DATE + "?day=" + date;

        // Depurando objeto Json...
        //Log.d(TAG, jobject.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.GET,
                        newURL,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                // Procesar la respuesta del servidor
                                procesarRespuesta3(response);


                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );




    }


       public void aceptar(View v) {



        date = date_String.getText().toString().trim();

        map2 = new HashMap<>();// Mapeo previo

        map2.put("day", date);
        map2.put("search_worker_id", id);
        map2.put("search_worker_work_place_id", work_place_id);
        map2.put("offer_worker_id1", "");
        map2.put("offer_worker_work_place_id1","");

        // Crear nuevo objeto Json basado en el mapa
        jobject2 = new JSONObject(map2);

        // Depurando objeto Json...
        Log.d(TAG, jobject2.toString());

        // Actualizar datos en el servidor
        VolleySingleton.getInstance(this).addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        INSERT_DATE,
                        jobject2,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Procesar la respuesta del servidor
                                procesarRespuesta2(response);
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Error Volley: " + error.getMessage());
                            }
                        }

                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<String, String>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );





    }

    private void procesarRespuesta(JSONObject response) {

        try {
            // Obtener atributo "mensaje"
            String mensaje = response.getString("estado");

            switch (mensaje) {
                case "1":
                    // Obtener objeto "meta"
                    JSONObject object = response.getJSONObject("ids");

                    //Parsear objeto
                   ids = gson.fromJson(object.toString(), Ids.class);

                    id = ids.getId();
                    work_place_id = ids.getWork_place_id();



                    break;

                case "2":
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;

                case "3":
                    String mensaje3 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje3,
                            Toast.LENGTH_LONG).show();
                    break;
            }



        } catch (JSONException e) {

            e.printStackTrace();
        }
          //return ids;
    }

    private void procesarRespuesta2(JSONObject response) {

        try {
            // Obtener estado
            String estado = response.getString("estado");
            // Obtener mensaje
            String mensaje = response.getString("mensaje");

            switch (estado) {
                case "1":
                    //openProfile();
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de éxito
                    //this.setResult(Activity.RESULT_OK);
                    // Terminar actividad
                    //this.finish();
                    //openProfile();
                    break;

                case "2":
                    // Mostrar mensaje
                    Toast.makeText(
                            this,
                            mensaje,
                            Toast.LENGTH_LONG).show();
                    // Enviar código de falla
                    this.setResult(Activity.RESULT_CANCELED);
                    // Terminar actividad
                    this.finish();
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void procesarRespuesta3(JSONObject response) {

        try {
            // Obtener atributo "mensaje"
            String mensaje = response.getString("estado");

            switch (mensaje) {
                case "1":
                    // Obtener objeto "meta"
                    JSONObject object = response.getJSONObject("search_day");

                    //Parsear objeto
                    sday = gson.fromJson(object.toString(), SearchDay.class);

                    id = ids.getId();
                    work_place_id = ids.getWork_place_id();

                    id_searchday = sday.getId();
                    day = sday.getDay();
                    search_worker_id = sday.getSearch_worker_id();
                    search_worker_work_place_id = sday.getSearch_worker_work_place_id();
                    offer_worker_id1 = id;
                    offer_worker_work_place_id1 = work_place_id;

                    Intent i = new Intent(this, HacerDia.class);
                    i.putExtra("id", id_searchday);
                    i.putExtra("day", day);
                    i.putExtra("search_worker_id", search_worker_id);
                    i.putExtra("search_worker_work_place_id", search_worker_work_place_id);
                    i.putExtra("offer_worker_id1", offer_worker_id1);
                    i.putExtra("offer_worker_work_place_id1", offer_worker_work_place_id1);
                    startActivity(i);

                    /*Toast toast = Toast.makeText(this, id, Toast.LENGTH_SHORT);
                    toast.show();
                    Toast toasta = Toast.makeText(this, work_place_id, Toast.LENGTH_SHORT);
                    toasta.show();*/

                    // String id = ids.getId();
                    // String work_place_id = ids.getWork_place_id();

                    // ids2 = new Ids(id,work_place_id);
                    // return ids2;

                    // Asignar color del fondo
                   /* switch (ids.getId()) {
                        case "Salud":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.saludColor));
                            break;
                        case "Finanzas":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.finanzasColor));
                            break;
                        case "Espiritual":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.espiritualColor));
                            break;
                        case "Profesional":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.profesionalColor));
                            break;
                        case "Material":
                            cabecera.setBackgroundColor(getResources().getColor(R.color.materialColor));
                            break;
                    }

                    // Seteando valores en los views
                    titulo.setText(meta.getTitulo());
                    descripcion.setText(meta.getDescripcion());
                    prioridad.setText(meta.getPrioridad());
                    fechaLim.setText(meta.getFechaLim());
                    categoria.setText(meta.getCategoria());*/

                    break;

                case "2":
                    String mensaje2 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje2,
                            Toast.LENGTH_LONG).show();
                    break;

                case "3":
                    String mensaje3 = response.getString("mensaje");
                    Toast.makeText(
                            this,
                            mensaje3,
                            Toast.LENGTH_LONG).show();
                    break;
            }

            //return ids2;


        } catch (JSONException e) {

            e.printStackTrace();
        }
        //return ids;
    }

    public void listaDiasPedidos(View v){
        Intent intent = new Intent(this, ListaDiasPedidos.class);
        intent.putExtra("username", username);
        startActivity(intent);
    }


}
