package david.es.project;

/**
 * Created by David on 24/5/16.
 */
public class ProvideDay {

    String id;
    String day;
    String provide_worker_id;
    String provide_worker_work_place_id;
    String search_worker_id;
    String search_worker_wprk_place_id;

    public ProvideDay() {
    }

    public ProvideDay(String id, String day, String provide_worker_id, String provide_worker_work_place_id, String search_worker_id, String search_worker_wprk_place_id) {
        this.id = id;
        this.day = day;
        this.provide_worker_id = provide_worker_id;
        this.provide_worker_work_place_id = provide_worker_work_place_id;
        this.search_worker_id = search_worker_id;
        this.search_worker_wprk_place_id = search_worker_wprk_place_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getProvide_worker_id() {
        return provide_worker_id;
    }

    public void setProvide_worker_id(String provide_worker_id) {
        this.provide_worker_id = provide_worker_id;
    }

    public String getProvide_worker_work_place_id() {
        return provide_worker_work_place_id;
    }

    public void setProvide_worker_work_place_id(String provide_worker_work_place_id) {
        this.provide_worker_work_place_id = provide_worker_work_place_id;
    }

    public String getSearch_worker_id() {
        return search_worker_id;
    }

    public void setSearch_worker_id(String search_worker_id) {
        this.search_worker_id = search_worker_id;
    }

    public String getSearch_worker_wprk_place_id() {
        return search_worker_wprk_place_id;
    }

    public void setSearch_worker_wprk_place_id(String search_worker_wprk_place_id) {
        this.search_worker_wprk_place_id = search_worker_wprk_place_id;
    }
}
